import { combineReducers }  from 'redux';

import userInfo from './userInfo';


export default combineReducers({
    userInfo
})

export const getUserdata = ({useInfo}) => userInfo