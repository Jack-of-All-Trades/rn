const userInfoDefaultState = {}

export default (state = userInfoDefaultState, {type, payload} = {}) => {

    switch(type) {
        case 'ADD_USER': 
        case 'REMOVE_USER': 
        case 'DELETE_USER' : 
        case 'GET_USER' :
        case 'GET_TOTAL':
        {
        return {
            ...state
         }
        }
        
        default: 
            return state;
    }
};