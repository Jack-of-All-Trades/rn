import {createStore, applyMiddleware, compose} from 'redux'
import thunk from 'redux-thunk'
import mainReducer from '../reducers'

export default function ReduxStore (initialState) {
  const enhancer = compose(
    applyMiddleware(thunk)
  )
  return createStore(mainReducer, initialState, enhancer)
}
