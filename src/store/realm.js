import Realm from 'realm'
import { ListView } from 'realm/react-native';
const uuidv1 = require('uuid/v1');

export class Userdata {

    static get () {
        return realm.objects(Userdata.schema.name);
    };

    static schema = {

        name: 'Userdata',
        primaryKey: 'id',
        properties : {
            id: {type: 'string' },
            name: {type: 'string'},
            email: {type: 'string'},
            dob: {type: 'string'},
            createdAt: {type: 'date'}
        }

    };
};

export const userdataDS = new ListView.DataSource({rowHasChanged: (r1, r2) => r1.id !== r2.id});


//get all users data
export const getUserdata = () => {
    const userdata = Userdata.get().sorted('createdAt', true);
    return userdata
}


//get individual user data
export const getUser = (id) => {
    const user = realm.objectForPrimaryKey(Userdata, id);
    return user
}

//get total count
export const getTotal = () => {
  let items = Array.from(realm.objects('Userdata'));
  return items;
}

//create new user
export const createUser =  (name, email, dob) => {

    realm.write(()=> {
        realm.create(Userdata.schema.name, {
            id: uuidv1(),
            name,
            email,
            dob,
            createdAt: new Date()
        })
    })
}


// Wipe out
export const DeleteAll = () => {
    realm.write(() => {
        realm.deleteAll()
    })
}


//delete user 
export const deleteUser = (user) => {

    realm.write(() => {
        realm.delete(user)
    })
}

const realm = new Realm ({schema: [Userdata]})