import { connect } from 'react-redux';

import store from '../store';

import * as actions from '../actions/actions';

import MainForm from './MainForm';

import { getUserdata } from '../reducers';

const UserResults = store.getUserdata();

const mapStateToProps = (state, props) => ({

    ...getUserdata(state),
    dataSource: store.userdataDS.cloneWithRows(UserResults)
})

const mapDispatchToProps = {
    ...actions
};


export default connect(mapStateToProps, mapDispatchToProps)(MainForm);