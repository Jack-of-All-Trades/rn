import React from 'react';

import { View, Text } from 'react-native';

import { Button } from './';


const AdminBtn = (props) => {

        return (
            <View style={styles.loginbtnStyles}>
            <Button 
            label={props.label}
            onPress={props.onPress}
            />
        </View>

        );
}

const styles = {

    loginbtnStyles: {
        alignSelf: 'flex-end',
        marginTop: 30,
        padding: 5,
        position: 'absolute',

    }
}


export default AdminBtn;