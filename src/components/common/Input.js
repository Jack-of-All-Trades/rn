import React from 'react';
import { View, TextInput, Text} from 'react-native';


const Input = (props) => {

    const { containerStyles, textStyles, inputStyles } = styles;

    return (

        <View style={containerStyles}>
        <Text style={textStyles}> {props.label} </Text>
        <TextInput
        placeholder={props.placeholder}
        autoCorrect={false}
        value={props.value}
        onChangeText={props.onChangeText}
        onChange={props.onChange}
        secureTextEntry={props.secureTextEntry}
        keyboardType={props.keyboardType}
        style={inputStyles}
        />
        
        </View>
    );

}; 

const styles = {

    containerStyles: {
        flex: 1,
        height: 50,
        flexDirection: 'row',
        alignItems: 'center'
    },
    textStyles: {
        paddingLeft: 10,
        flex: 1,
        fontSize: 18
    },
    inputStyles: {
        color: '#000',
        paddingLeft: 10,
        paddingRight: 5,
        flex: 2,
        fontSize: 18,
        lineHeight: 20
    }
};

export  { Input };