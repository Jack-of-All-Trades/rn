import React from 'react';

import { View, Image} from 'react-native';


const Header = () => {

    return (
    <View style={styles.viewStyles}> 
        <Image style={styles.imageStyles} source={require('../../assets/logo.png')} />
    </View> 
    );
};


const styles = {

    viewStyles: {
        justifyContent: 'center',
        paddingTop: 30,
        paddingBottom: 30,
        alignItems: 'center',
    },
    imageStyles: {
    flexGrow:1,
     alignItems: 'center',
     justifyContent:'center',
    }
};

export { Header }; 