import React from 'react';

import { Text, TouchableOpacity } from 'react-native';


const Button = (props) => {

    const { textStyles, buttonStyles } = styles;

    return (
        <TouchableOpacity onPress={props.onPress} style={buttonStyles}>

        <Text style={textStyles}>
            {props.label}
        </Text>
        
        </TouchableOpacity>
    );
}


const styles = {

    textStyles: {
        fontSize: 18,
        alignSelf: 'center',
        paddingTop: 10,
        paddingBottom: 10,
        paddingLeft: 10,
        paddingRight: 10,
        color: '#f04084'
    },

    buttonStyles: {
        borderColor: "#f04084",
        borderWidth: 1,
        borderRadius: 5,
        marginLeft: 5,
        marginRight: 5,
    },

}

export { Button };
