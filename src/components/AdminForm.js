import React, { Component } from 'react';

import { View, TextInput, Text, Modal } from 'react-native';

import { Container, ContainerSection, Button, Input } from './common';



class AdminForm extends Component {

    constructor(props) {
    super(props); 
    this.state = {
        username: '',
        password: '',
        erroruser: '',
        errorpassword: '',
        modalVisible: false,
        invalid: false
    };
    
    // this.formfieldUpdate = this.formfieldUpdate.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
}   

//     formfieldUpdate(fieldName) {
//         return (event) => {
//         this.setState(() => ({
//             [fieldName]: event.nativeEvent.text
//         }))
//     }
// }  

    userNameChange = (username) => {
        // const username = event.nativeEvent.text;
        this.setState( () => ({
            username
        }))
    } 


    onpassWordChange = (password) => {

        // const password = event.nativeEvent.text;

        this.setState(() => ({
            password
        }))
    }

    setModalVisible(visible) {
        this.setState( () => ({
            modalVisible: visible
        }));
    }

    checkValidation() {

        let invalid = false;

        const { username, password} = this.state;

        let erroruser = ''
        let regusername = /^[A-Za-z0-9_ ]+$/;
        if (!!this.state.username == '' || this.state.username !== 'Admin') {
             erroruser = 'Please enter correct username',
             invalid = true
         }
         let errorpassword = ''
         if (!!this.state.password == '' || this.state.password !== 'password') {
             errorpassword = 'Please enter correct password',
             invalid = true
        } 


        this.setState(() => ({
            erroruser,
            errorpassword,
            invalid
        }))

        return invalid;

    }
    onSubmit = () => {
        let invalid = this.checkValidation();

        // let usernnameInput = this.refs.usernameInput._lastNativeText;

        // let passwordInput = this.refs.passwordInput._lastNativeText;

        if (!invalid && this.state.username == 'Admin' && this.state.password == 'password') {

            this.props.navigation.navigate("Details", {screen: "AdminDetailPage"})
            
            this.setState(() => ({
                username: '',
                password: ''
            }))
        }

    }


    render() {
        return (
        <View>
         <Container>
         <ContainerSection>
            <Input 
              label="Admin"
              placeholder="Username"
              value={this.state.username}   
              onChangeText={this.userNameChange.bind(this)}       
            />
            </ContainerSection>
            <ContainerSection> 
            <Input
            label="Password"
            placeholder="*******"
            value={this.state.password}
            secureTextEntry={true}
            onChangeText={this.onpassWordChange.bind(this)}
            />
            </ContainerSection>
            </Container> 
            <Text style={styles.errortextStyles}>
            {this.state.erroruser}
            </Text>
           <Text style={styles.errortextStyles}>
           {this.state.errorpassword}
           </Text>
           <View style={styles.centerStyles}>
           <ContainerSection> 
            <Button
            label="submit"
            onPress={this.onSubmit}
            />
        </ContainerSection>
        </View>
        </View>
        );  
    }
}
const styles = {

    errortextStyles: {
        fontSize: 15,
        alignItems: 'center',
        textAlign: 'center',
        color: 'red',
        paddingTop: 10,
        paddingBottom: 10
    },

    containerStyles: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    modalcontainerStyles: {
        justifyContent: 'center',
        alignContent: 'center',
        borderRadius: 4,
        borderColor: 'rgba(0, 0, 0, 0.1)',
    },

    centerStyles: {
        alignItems: 'center',
        justifyContent: 'center',
    }

};  


export default AdminForm;