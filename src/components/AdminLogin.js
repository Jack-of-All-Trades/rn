import React from 'react';

import { View } from 'react-native';

import AdminForm from './AdminForm';

import  { Header } from './common';


class AdminLogin extends React.Component {
    constructor(props){
        super(props);
    }
    render() {
        return (

            <View style={{ backgroundColor: "white", flex:1}}> 
            <Header/>
            <AdminForm navigation={this.props.navigation}/>
            </View>
        );
    }
};

export default AdminLogin;

