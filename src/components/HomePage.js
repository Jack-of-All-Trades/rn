import React, { Component } from 'react';
import { View } from 'react-native';
import { Provider } from 'react-redux';
import MainForm from './MainForm';
import { Header } from './common';
import AdminBtn from './common/AdminBtn';

class HomePage extends Component {
    render(){
        const { navigate } = this.props.navigation;
    return (
        <View style={{ backgroundColor: "white", flex:1}}>  
                <AdminBtn 
                label="login"
                onPress={ () => navigate("Login", { screen: "AdminLogin"} )}
                />
                <Header />                                         
                <MainForm />
        </View>        

        );
    }  
};


export default HomePage;