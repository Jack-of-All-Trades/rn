import React, { Component } from 'react';

import { View, Text, Modal, ScrollView, NetInfo, ToastAndroid, TextInput } from 'react-native';

import { Container, ContainerSection, Button } from './common';

import { Buffer } from 'buffer';

const Realm = require('realm');

import { DeleteAll, getTotal } from '../actions/actions';


// import { RNFS } from 'react-native-fs';

import RNFetchBlob from 'rn-fetch-blob';


var csvfilepath = `${RNFetchBlob.fs.dirs.PictureDir}/data.csv`;

export default class AdminDetail extends Component {

    constructor(props){
        super(props);
        this.state = {
            modalVisible: false,
            connectioninfo: '',
            size: 0
        };
        this.ConnectivityChange = this.ConnectivityChange.bind(this);
    }



    componentWillMount(){
        Realm.open({}).then(realm => {
           this.setState({ size: realm.objects('Userdata').length})
        });
    }


    componentWillUnmount() {
        NetInfo.removeEventListener(
            'connectionChange',
            //ConnectivityChange
          );
    }


    setModalVisible(visible) {
        this.setState( () => ({
            modalVisible: visible
        }));
    }

    onDeleteAll = () => {

        this.setModalVisible(true);
    }

    onYesdeleteAll = () => {

        DeleteAll();

        this.setModalVisible(false);
    }

    onEmailMeClicked = () => {

        Realm.open({}).then(realm => {
                let items = Array.from(realm.objects('Userdata'));
                let total = items.length;
                console.log(total);
                var csv = "id,name,email,dob,createdAt\n";
                for(var i in items) {
                  csv += items[i].id+","+items[i].name+","+items[i].email+","+ items[i].dob+","+items[i].createdAt+"\n"
                }
  
                RNFetchBlob.fs.writeFile(csvfilepath, csv, 'utf8')
                .then(() => {
                    console.log(csv);
                })
                .catch((err) => {
                    ToastAndroid.show("Error Generating CSV File",ToastAndroid.LONG);
                })

                let csvcontent = Buffer.from(csv).toString('base64');

                NetInfo.getConnectionInfo().then((connectionInfo) => {
                    this.setState({
                        connectionInfo: connectionInfo.type
                    })
                    //console.log('Initial, type: ' + connectionInfo.type + ', effectiveType: ' + connectionInfo.effectiveType);
                    //var newcsvfile = utf8.encode(`${csvfilepath}`);
                    //let csvcontent = Buffer.from(csv).toString('base64');
                    fetch('https://api.sendgrid.com/v3/mail/send', {
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/json',
                            'Authorization': ''
                        },
                        body: JSON.stringify({
                            "personalizations": [
                                {
                                    "to": [
                                        {
                                            "email": "123@test.com"
                                        }
                                    ],
                                    "subject": "Marketing SignUps."
                                }
                            ],
                            "from": {
                                "email": "123@test.com"
                            },
                            "content": [
                                {
                                    "type": "text/html",
                                    "value": "Please find the csv file as atatched."
                                }
                            ],

                            "attachments": [{
                                "content": csvcontent,
                                "type": "text/csv",
                                "disposition": "attachment",
                                "filename": "data.csv"

                            }]

                        }),
                    })
                        .then((response) => {
                            console.log(response);
                            ToastAndroid.show("Email has been delivered to you", ToastAndroid.LONG);
                        })
                        .catch(function (err) {
                            console.log(err);
                            ToastAndroid.show("Email has not been delivered. Please check internet connection.", ToastAndroid.LONG);
                            
                        })
                });

                NetInfo.addEventListener(
                    'connectionChange',
                    //ConnectivityChange
                );
          });  
      }

    ConnectivityChange(connectionInfo) {
        this.setState({
            connectionInfo: connectionInfo.type
        })
        console.log('First change, type: ' + connectionInfo.type + ', effectiveType: ' + connectionInfo.effectiveType);

    }

    render() {
        return(

            <View style={styles.containerStyles}>
                <View>
                <Button
                label="Delete all"
                onPress={this.onDeleteAll.bind(this)}
                />
                </View>
                <View>
                <Text style={{ fontSize: 20 }}> 
                Total No of Users
                </Text>
                <Text style={{ textAlign: 'center', fontSize: 20 }}>
                {this.state.size} 
                </Text>
                </View>
                <View>
                <Button
                label="Email me"
                onPress={this.onEmailMeClicked.bind(this)}
                />
                </View>
                <Modal
                visible={this.state.modalVisible}
                transparent={false} 
                >
                <View style={styles.innercontainerStyles}>
                <View style={styles.modalcontainerStyles}>
                <Text style={{ fontSize: 20}}> Are you sure you wanna delete all of the data? </Text>
                <View>
                <View style={{ marginTop: 20}}>
                <Button
                label="No"
                onPress={ () => {
                    this.setModalVisible(false);
                }}/>
                </View>
                </View>
                <View>
                <View style={{ marginTop: 50, justifyContent: 'flex-end'}}>
                <Button
                label="Yes"
                onPress={this.onYesdeleteAll.bind(this)} />
                </View>
                </View>
                </View>
                </View>
                </Modal>
            </View>
        );
    }
}


const styles = {
    containerStyles: {
        display: 'flex',
        justifyContent: 'space-around',
        flexDirection: 'row',
        marginTop: 50
    },
    innercontainerStyles: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    modalcontainerStyles: {
        justifyContent: 'center',
        alignContent: 'center',
        borderRadius: 4,
        borderColor: 'rgba(0, 0, 0, 0.1)',
    }
}
