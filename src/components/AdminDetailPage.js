import React, { Component } from 'react';
import { View } from 'react-native';
import AdminDetail from './AdminDetail';
import { Header} from './common';
import AdminBtn from './common/AdminBtn';


export default class AdminDetailPage extends Component {

    render() {
        const { goBack } = this.props.navigation;
        return(

            <View style={{backgroundColor: 'white', flex:1}}>
               <AdminBtn
               label="logout" 
               onPress={ () => goBack() }
              />
                <Header />
                <AdminDetail />
            </View>
        );
    }
}


