import React, { Component } from 'react';

import { View, TextInput, Text, Modal, ScrollView, NetInfo, ToastAndroid } from 'react-native';

import DatePicker from 'react-native-datepicker';

import { Container, ContainerSection, Button, Input } from './common';

// import { ListView } from 'realm/react-native';


import { createUser, getUserdata } from '../actions/actions';


class MainForm extends Component {
    
    constructor(props) {
    super(props);
    this.state = {
        username: '',
        email: '',
        erroruser: '',
        erroremail: '',
        invalid: false,
        modalVisible: false,
        dob: '',
    };
    this.sendData = this.sendData.bind(this);
}   

    onUsernameChange = (username) => {

        this.setState( () => ({
            username 
        }));
    }

    onEmailChange = (email) => {

        this.setState( () => ({
            email
        }));
    };




    checkValidation = () => {
        let invalid = false;

        const { username, email } = this.state;
        
        let errorusername = '';
        let regusername = /^[A-Za-z_ ]+$/;

        if(!!this.state.username == '' || !regusername.test(username)) {
                errorusername = 'Please enter a valid username',
                invalid = true
        }
        
        let erroremail = '';
        let regemail = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;    
        if(!this.state.email || !regemail.test(email)){
              erroremail = 'Please enter a valid email address',
            invalid = true
        }

        this.setState( () => ({
            erroremail,
            errorusername,
            invalid
        }));

        return invalid


    }

    onDobChange = (dob) => {

        this.setState( () => ({
            dob
        }))
    }

    setModalVisible(visible) {
        this.setState( () => ({
            modalVisible: visible
        }));
    }
    
    sendData = () => {

            const userdata = {
            username: this.state.username,
            email: this.state.email,
            dob:  this.state.dob
            }

            const name = JSON.stringify(userdata.username);
            const email = JSON.stringify(userdata.email);
            const dob = JSON.stringify(userdata.dob);

             createUser(name, email, dob);

            this.setState( () => ({

                username: '',
                email: '',
                dob: '',
                errorusername: '',
                erroremail: ''
            }))

    };
    onButtonClicked = () => {
        var createUser = this.props;   
        let invalid = this.checkValidation();
        if(!invalid) {

            this.setModalVisible(true);

            this.sendData();

        }

    };

    render() {
        const { dataSource, deleteUser } = this.props;
        return (
        <View>
         <Container>
         <ContainerSection>
            <Input 
             label="Full Name"
              placeholder="Your Name"
              value={this.state.username}
              onChangeText={this.onUsernameChange.bind(this)}              
            />
            </ContainerSection>
            <ContainerSection> 
            <Input
            label="Email"
            placeholder="youremail@domain.com"
            value={this.state.email}
             keyboardType='email-address'
            onChangeText={this.onEmailChange.bind(this)}
            />
            </ContainerSection>
            <ContainerSection>
            <Text style={styles.doblabelStyles}>DOB </Text>
            <DatePicker
            style={styles.datepickerStyles}
            date={this.state.dob}
            mode="date"
            placeholder="Date of Birth"
            confirmBtnText="Confirm"
            cancelBtnText="Cancel"
            format="DD-MM-YYYY"
            onDateChange={this.onDobChange.bind(this)}
            />
            </ContainerSection>
            </Container> 
            <Text style={styles.errortextStyles}>
            {this.state.errorusername}
            </Text>
           <Text style={styles.errortextStyles}>
           {this.state.erroremail}
           </Text>
           <View style={{ justifyContent:'center', alignItems:'center', marginTop: 20}}>
            <Button
            label="Submit"
            onPress={this.onButtonClicked.bind(this)}
            />
            </View>
        <Text style={styles.tncStyles} numberOfLines={3}> By submitting an entry you allow Daily Vanity Pte Ltd to contact you directly via email for the latest beauty updates on a weekly basis. </Text>
        <Modal
        visible={this.state.modalVisible}
        transparent={false}
        >
        <View style={styles.containerStyles}>
        <View style={styles.modalcontainerStyles}>
        <Text> Thank You for Registering!!! </Text>
        <View style={{ marginTop: 30,  alignItems: 'center', justifyContent: 'center' }}>
        <ContainerSection>
        <Button 
        label="close"
        onPress={ () => {
            this.setModalVisible(false);
        } }    
      />
      </ContainerSection>
      </View>
      </View>
      </View>
        </Modal>
        </View>
        );  
    }
}
const styles = {

    errortextStyles: {
        fontSize: 15,
        alignItems: 'center',
        textAlign: 'center',
        color: 'red'
    },

    containerStyles: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    modalcontainerStyles: {
        justifyContent: 'center',
        alignContent: 'center',
        borderRadius: 4,
        borderColor: 'rgba(0, 0, 0, 0.1)',
    },

    tncStyles: {

        fontSize: 15,
        alignItems: 'center',
        textAlign: 'center',
        color: 'black',
        marginTop: 20
    },

    doblabelStyles: {
        paddingLeft: 13,
        paddingTop: 10,
        flex: 1,
        fontSize: 18,
        height: 50,
        flexDirection: 'row',
        alignItems: 'center'
    },

    datepickerStyles: {
        paddingLeft: 10,
        paddingRight: 5,
        flex: 2,
        paddingTop: 5

    },

    centerStyles: {
        alignItems: 'center',
        justifyContent: 'center'
    }
};  

export default MainForm;