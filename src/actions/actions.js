import store from '../store';


//ADD User
export function createUser(name, email, dob) {
    store.createUser(name, email, dob);
    return {
        type: 'ADD_USER'
    }
}

//Remove User

export function deleteUser(user) {
    store.deleteUser(user);
    return{
        type: 'REMOVE_USER'
    }

}


export function getUserdata() {
    store.getUserdata();
    return {
        type: 'GET_USER'
    }
}

// Delete all Users 
export function DeleteAll() {
    store.DeleteAll();
    return{
        type: 'DELETE_USER'
    }
}

export function getTotal() {
    store.getTotal();
    return {
        type: 'GET_TOTAL'
    }
}