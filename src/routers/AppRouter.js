import React from 'react';
import { StackNavigator } from 'react-navigation';
import HomePage from '../components/HomePage';
import AdminLogin from '../components/AdminLogin';
import AdminDetailPage from '../components/AdminDetailPage';

export default AppRouter  = StackNavigator({
        Home: { screen: HomePage },
        Login: { screen: AdminLogin },
        Details: {screen: AdminDetailPage},
},
{
navigationOptions: {
    header: null,
  },
}
);
