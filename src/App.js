import React, { Component } from 'react';
import { AppRegistry } from 'react-native';
import { Provider } from 'react-redux'
import AppRouter from './routers/AppRouter';
import ReduxStore from './store/ReduxStore';
import CodePush from 'react-native-code-push';

const reduxstore = ReduxStore();
const CodePushConfig = {
    checkFrequency: CodePush.CheckFrequency.ON_APP_RESUME, 
    installMode: CodePush.InstallMode.IMMEDIATE 
};
export default class AppPush extends Component{
    constructor (props) {
        super (props);
    };

    componentWillMount() {

        CodePush.sync(CodePushConfig);
        // Analytics.trackEvent('Code push checking for updates');
    }

    render() {
        return(
            <Provider store={reduxstore}>
            <AppRouter />       
            </Provider>
        );
    };
};


const App = CodePush(CodePushConfig)(AppPush);

// Analytics.trackEvent('App loaded', App);

AppRegistry.registerComponent('Marketing', () => App);
